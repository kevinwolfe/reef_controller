#ifndef _HARDWARE_H_
#define _HARDWARE_H_

#include <stdbool.h>

#define INVALID_HARDWARE_HANDLE   ((uint8_t)-1)

typedef enum
{
  LASER_LEVEL_0,  LASER_LEVEL_DT = LASER_LEVEL_0,
  LASER_LEVEL_1,  LASER_LEVEL_QT = LASER_LEVEL_1,
  LASER_LEVEL_2,
} laser_level_t;

typedef enum
{
  RELAY_CTL_0,
  RELAY_CTL_1,  RELAY_CTL_DT_DUMP_PUMP      = RELAY_CTL_1,
  RELAY_CTL_2,  RELAY_CTL_QT_DUMP_PUMP      = RELAY_CTL_2,
  RELAY_CTL_3,  RELAY_CTL_DT_DUMP_VALVE     = RELAY_CTL_3,
  RELAY_CTL_4,  RELAY_CTL_QT_FILL_SOLENOID  = RELAY_CTL_4,
  RELAY_CTL_5,  RELAY_CTL_DT_FILL_SOLENOID  = RELAY_CTL_5,
} relay_ctl_t;

typedef enum
{
  FET_CTL_0,
  FET_CTL_1,
  FET_CTL_2,
  FET_CTL_3,
} fet_ctl_t;

typedef enum
{
  LEVEL_SENSOR_0,  LEVEL_SENSOR_DT = LEVEL_SENSOR_0,
  LEVEL_SENSOR_1,
  LEVEL_SENSOR_2,
  LEVEL_SENSOR_3,  LEVEL_SENSOR_QT = LEVEL_SENSOR_3,
  LEVEL_SENSOR_4,
  LEVEL_SENSOR_5,
} level_sensor_t;

typedef enum
{
  OPTICAL_SENSOR_0,
  OPTICAL_SENSOR_1,
} optical_sensor_t;

typedef enum
{
  AUX_IO_0,
  AUX_IO_1,
  AUX_IO_2,
  AUX_IO_3,  PIN_TEMP_SENSOR = AUX_IO_3,
} aux_io_t;

typedef enum
{
  DAC_OUT_0,
  DAC_OUT_1,
} dac_out_t;

void hardware_init();
bool hardware_user_button_pressed(void);
void hardware_turn_on_led(void);
void hardware_turn_off_led(void);
void hardware_toggle_led(void);

bool hardware_optical_sensor_is_wet(optical_sensor_t optical_sensor);
bool hardware_level_sensor_is_wet(level_sensor_t level_sensor);

void hardware_relay_close(relay_ctl_t relay_ctl);
void hardware_relay_open(relay_ctl_t relay_ctl);

double hardware_get_laser_level_mm( laser_level_t idx );

void hardware_fet_energize(fet_ctl_t fet_ctl);
void hardware_fet_open(fet_ctl_t fet_ctl);

double hardware_get_dac_max_voltage();
void   hardware_set_dac_voltage(dac_out_t dac_out, double voltage);

double hardware_get_latest_temp_reading_f();

#endif
