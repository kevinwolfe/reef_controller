#include <freertos/freertos.h>
#include <freertos/task.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "parson.h"
#include "debug.h"
#include "nvm.h"
#include "utils.h"
#include "application.h"
#include "hardware.h"
#include "wifi.h"

static const uint32_t ato_overfill_duration_before_close_s  =  5;         // Amount of time needed for a wet sensor before the solenoid opens
static const uint32_t ato_underfill_duration_before_open_s  = 15;         // Debounced sensor time before turning pump on
static const uint32_t solenoid_max_open_time_s              =  3 * 60;    // Maximum amount of time to keep the solenoid energized for
static const uint32_t solenoid_min_cooldown_time_s          = 15 * 60;    // Minimum amount of time between solenoid energize periods

//static const uint32_t wc_min_wet_period_before_pump_on_s  = 5;
//static const uint32_t wc_min_dry_period_before_pump_off_s = 5;

typedef enum
{
  DISPLAY_TANK,
  QUARANTINE_TANK,
  TANK_CNT,
} tank_id_t;

#define TANK_ID_TO_STR(tank_id)     ( tank_id == DISPLAY_TANK ? "DT" : "QT" )

#define DISPLAY_TANK_TARGET_WATER_LEVEL_CM       22.0
#define QUARANTINE_TANK_TARGET_WATER_LEVEL_CM    0    // 17.0
#define WATER_CHANGE_MARGIN_OPEN                 0.3
#define WATER_CHANGE_MARGIN_CLOSE                0.1

static const double s_laser_level_range_min_cm =  5.0;
static const double s_laser_level_range_max_cm = 50.0;


#define ATO_USE_LASER_LEVEL

//static double           s_water_levels[TANK_CNT] = { 0 };
//static pthread_mutex_t  s_state_mutex;

typedef struct
{
  bool         is_open;
  float        last_open_time_s;
  float        last_close_time_s;
  float        scheduled_open_time_s;
  float        scheduled_close_time_s;
  bool         initial_open;
  relay_ctl_t  relay_ctl;
} solenoid_state_t;

typedef struct
{
  bool              level_sensor_wet;
  double            water_level_cm;
  solenoid_state_t  fill_solenoid;
  
  relay_ctl_t       dump_pump_relay_ctl;
  bool              dump_pump_running;
  relay_ctl_t       dump_valve_relay_ctl;
  bool              dump_valve_is_open; 
  
  laser_level_t     laser_level;
  level_sensor_t    level_sensor;

  bool              water_change_request_start;
  bool              water_change_request_stop;
  float             water_change_request_duration_s;
  float             water_change_end_time_s;

  bool              manual_control_dump_pump_on_request;
  bool              manual_control_open_solenoid_request;
  bool              manual_control_dump_valve_open_request;
    
  double            target_water_level_cm;
} tank_state_t;

static tank_state_t s_tank[TANK_CNT] = { 0 };

static tank_state_t * const s_p_dt = &s_tank[DISPLAY_TANK];
static tank_state_t * const s_p_qt = &s_tank[QUARANTINE_TANK];

static bool  s_manual_control_enabled = false;

static void _open_fill_solenoid( tank_id_t tank_id );
static void _close_fill_solenoid( tank_id_t tank_id );

static void _open_dump_valve( tank_id_t tank_id );
static void _close_dump_valve( tank_id_t tank_id );

static void _turn_on_dump_pump( uint8_t tank_id );
static void _turn_off_dump_pump( uint8_t tank_id );

static void _application_task(void *Param);

static void _run_automatic_top_off(uint8_t tank_id);
static void _run_automatic_water_change(uint8_t tank_id);
static void _run_manual_control(uint8_t tank_id);

//-----------------------------------------------------------------------------
void application_init(void)
{
  xTaskCreate( _application_task, "app_task", 8192, NULL, 5, NULL);
}

//-----------------------------------------------------------------------------
static void _application_task(void *Param)
{ 
  const uint32_t task_delay_ms = 100;
   
  print( "Reef ATO Controller app running!\n" );

  s_p_dt->fill_solenoid.initial_open = true;
  s_p_dt->target_water_level_cm      = DISPLAY_TANK_TARGET_WATER_LEVEL_CM; 
  s_p_dt->fill_solenoid.relay_ctl    = RELAY_CTL_DT_FILL_SOLENOID;
  s_p_dt->dump_pump_relay_ctl        = RELAY_CTL_DT_DUMP_PUMP;
  s_p_dt->dump_valve_relay_ctl       = INVALID_HARDWARE_HANDLE; //RELAY_CTL_DT_DUMP_VALVE;
  s_p_dt->level_sensor               = LEVEL_SENSOR_DT;
  s_p_dt->laser_level                = LASER_LEVEL_DT;

  s_p_qt->fill_solenoid.initial_open = true;
  s_p_qt->fill_solenoid.relay_ctl    = RELAY_CTL_QT_FILL_SOLENOID;
  s_p_qt->target_water_level_cm      = QUARANTINE_TANK_TARGET_WATER_LEVEL_CM;
  s_p_qt->dump_pump_relay_ctl        = RELAY_CTL_QT_DUMP_PUMP;
  s_p_qt->dump_valve_relay_ctl       = INVALID_HARDWARE_HANDLE;
  s_p_qt->level_sensor               = LEVEL_SENSOR_QT;
  s_p_qt->laser_level                = LASER_LEVEL_QT;
   
  _turn_off_dump_pump( DISPLAY_TANK );
  _turn_off_dump_pump( QUARANTINE_TANK );

  for ( tank_id_t tank_id = 0; tank_id < TANK_CNT; tank_id++ )
  {
    _close_fill_solenoid( tank_id );
    _close_dump_valve( tank_id );
  }

  while(1)
  {
    delay_ms(task_delay_ms);
    hardware_toggle_led();
    
    for ( uint8_t tank_id = 0; tank_id < TANK_CNT; tank_id++ )
    {
      tank_state_t *p_tank = &s_tank[tank_id];
      p_tank->level_sensor_wet = hardware_level_sensor_is_wet(s_tank[tank_id].level_sensor);
      p_tank->water_level_cm   = hardware_get_laser_level_mm(s_tank[tank_id].laser_level) / 10;
      
      if ( s_manual_control_enabled )
      {
        _run_manual_control(tank_id);
      }
      else if ( p_tank->water_change_request_start || 
                p_tank->water_change_request_stop  || 
                p_tank->water_change_end_time_s > 0 )
      {
        _run_automatic_water_change(tank_id);
      }
      else
      {
        _run_automatic_top_off(tank_id);
      }
    }
  }   
}

//-----------------------------------------------------------------------------
static void _run_manual_control(uint8_t tank_id)
{
  tank_state_t      *p_tank     = &s_tank[tank_id];
  solenoid_state_t  *p_solenoid = &s_tank[tank_id].fill_solenoid;

  p_solenoid->scheduled_open_time_s = 0;
  p_solenoid->scheduled_close_time_s = 0;

  (  p_tank->manual_control_open_solenoid_request  && !p_solenoid->is_open ) ? _open_fill_solenoid( tank_id ) : NULL;
  ( !p_tank->manual_control_open_solenoid_request  &&  p_solenoid->is_open ) ? _close_fill_solenoid( tank_id ) : NULL;
  (  p_tank->manual_control_dump_valve_open_request && !p_tank->dump_valve_is_open ) ? _open_dump_valve( tank_id ) : NULL;
  ( !p_tank->manual_control_dump_valve_open_request &&  p_tank->dump_valve_is_open ) ? _close_dump_valve( tank_id ) : NULL;
  (  p_tank->manual_control_dump_pump_on_request && ( !p_tank->dump_pump_running  ) ) ? _turn_on_dump_pump( tank_id )  : NULL;
  ( !p_tank->manual_control_dump_pump_on_request && (  p_tank->dump_pump_running ) ) ? _turn_off_dump_pump( tank_id ) : NULL;
}

//-----------------------------------------------------------------------------
static void _run_automatic_water_change(uint8_t tank_id)
{
  float now = system_uptime_s();
  
  tank_state_t      *p_tank     = &s_tank[tank_id];
  solenoid_state_t  *p_solenoid = &s_tank[tank_id].fill_solenoid;

  if ( p_tank->water_change_request_stop || ( ( p_tank->water_change_end_time_s > 0 ) && ( now >= p_tank->water_change_end_time_s ) ) )
  {
    print( "Water Change mode stopping\n" );
    p_tank->water_change_end_time_s   = 0;
    p_tank->water_change_request_stop = false;

    ( p_tank->dump_pump_running )  ? _turn_off_dump_pump( tank_id ) : NULL;
    ( p_tank->dump_valve_is_open ) ? _close_dump_valve( tank_id )   : NULL;
    return;
  }
  
  if ( p_tank->water_change_request_start )
  {
    p_tank->water_change_end_time_s    = now + p_tank->water_change_request_duration_s;
    p_tank->water_change_request_start = false;
    print( "Tank %i water change starting, ending in %i seconds\n", tank_id, p_tank->water_change_request_duration_s );
  }
  
  // Always keep the fill solenoid closed
  if ( p_solenoid->is_open )
  {
    _open_fill_solenoid( tank_id );
  }
  p_solenoid->scheduled_open_time_s = 0;
  p_solenoid->scheduled_close_time_s = 0;

  bool open_everything = false;
  bool close_everything = false;

  // Sanity check the water levels before acting on them
  if ( ( p_tank->water_level_cm < s_laser_level_range_min_cm ) || ( p_tank->water_level_cm > s_laser_level_range_max_cm ) )
  {
    close_everything = true;
  }
  else
  {
    if ( p_tank->water_level_cm >= ( p_tank->target_water_level_cm + WATER_CHANGE_MARGIN_OPEN ) )
    {
      open_everything = true;
    }
    
    if ( p_tank->water_level_cm <= ( p_tank->target_water_level_cm + WATER_CHANGE_MARGIN_CLOSE ) )
    {
      close_everything = true;
    }
  }

  if ( open_everything )
  {
    ( !p_tank->dump_pump_running )  ? _turn_on_dump_pump( tank_id ) : NULL;
    ( !p_tank->dump_valve_is_open ) ? _open_dump_valve( tank_id )   : NULL;
  }
  
  if ( close_everything )
  {
    ( p_tank->dump_pump_running )  ? _turn_off_dump_pump( tank_id ) : NULL;
    ( p_tank->dump_valve_is_open ) ? _close_dump_valve( tank_id )   : NULL;
  }
}

//-----------------------------------------------------------------------------
static void _run_automatic_top_off(uint8_t tank_id)
{
  float now = system_uptime_s();
  
  tank_state_t      *p_tank     = &s_tank[tank_id];
  solenoid_state_t  *p_solenoid = &s_tank[tank_id].fill_solenoid;
  
  if ( p_tank->dump_valve_is_open )
  {
    _close_dump_valve( tank_id );
  }
  
  if ( p_tank->dump_pump_running )
  {
    _turn_off_dump_pump( tank_id );
  }
  
  static bool water_level_too_low = false;
  
#ifdef ATO_USE_LASER_LEVEL 
  // Ensure the level measurements are reporting sensible values:
  if ( ( p_tank->water_level_cm > s_laser_level_range_min_cm ) && ( p_tank->water_level_cm < s_laser_level_range_max_cm ) )
  {
    water_level_too_low = ( p_tank->water_level_cm < p_tank->target_water_level_cm );
  }
#else
  water_level_too_low = !p_tank->level_sensor_wet;
#endif

  if ( water_level_too_low )
  {
    if ( !p_solenoid->is_open && !p_solenoid->scheduled_open_time_s )
    {
      uint32_t cooldown_time_s = ( p_solenoid->initial_open ? 10 : solenoid_min_cooldown_time_s );       // 10 second delay on start
      p_solenoid->scheduled_open_time_s = MAX(  now + ato_underfill_duration_before_open_s, p_solenoid->last_close_time_s + cooldown_time_s );
      
      print( "solenoid %i open scheduled for: %f (in %f seconds)\n", tank_id, p_solenoid->scheduled_open_time_s, p_solenoid->scheduled_open_time_s - now );
    }
    
    if ( !p_solenoid->is_open && ( now > p_solenoid->scheduled_open_time_s ) )
    {
      printf("Opening solenid %i\n", tank_id );
      p_solenoid->scheduled_open_time_s = 0;
      _open_fill_solenoid( tank_id );
      p_solenoid->initial_open = false;
    }

    if ( p_solenoid->is_open && ( now > ( p_solenoid->last_open_time_s + solenoid_max_open_time_s ) ) )
    {
      print( "ERROR: Max solenoid %i open time limit hit!\n", tank_id );
      _close_fill_solenoid( tank_id );
      p_solenoid->scheduled_close_time_s = 0;
    }
  }
  else // !water_level_too_low
  {
    if ( p_solenoid->scheduled_open_time_s )
    {
      print( "Canceling scheduled open for %i\n", tank_id );
      p_solenoid->scheduled_open_time_s = 0;
    }

    if ( p_solenoid->is_open && !p_solenoid->scheduled_close_time_s )
    {
      p_solenoid->scheduled_close_time_s = now + ato_overfill_duration_before_close_s;
      print( "solenoid %i close scheduled for: %f (in %f seconds)\n", tank_id, p_solenoid->scheduled_close_time_s, p_solenoid->scheduled_close_time_s - now );
    }

    if ( p_solenoid->is_open && ( now > p_solenoid->scheduled_close_time_s ) )
    {
      printf("Closing solenid %i\n", tank_id );
      _close_fill_solenoid( tank_id );
      p_solenoid->scheduled_close_time_s = 0;
    }
  }
}


//-----------------------------------------------------------------------------
char const * application_get_html( const char *p_custom_header )
{
  static char buffer[2048] = { 0 };
  char *p_buffer = buffer;
  
  if ( p_custom_header )
  {
    p_buffer += sprintf(p_buffer, "%s", p_custom_header);
  }
  
  p_buffer += sprintf(p_buffer, "<h1>System Info</h1>");
  p_buffer += sprintf(p_buffer, "System Time: %s<br>", get_system_time_str());
  p_buffer += sprintf(p_buffer, "Firmware Build: %s %s, Boot Count: %li<br>", __DATE__, __TIME__, nvm_get_param_int32( NVM_PARAM_RESET_COUNTER ));
  p_buffer += sprintf(p_buffer, "Up-time: ");
  p_buffer += add_formatted_duration_str( p_buffer, system_uptime_s() );
  p_buffer += sprintf(p_buffer, "<br>");
  
  return buffer;
}

//-----------------------------------------------------------------------------
char const * application_post_html(const char *p_post_data)
{
  return application_get_html( NULL );
}

//-----------------------------------------------------------------------------
char const * application_get_mqtt_status_msg(void)
{
  static char status_msg[256];
  static uint32_t msg_id = 0;
  
  char *p_msg = status_msg;
  msg_id++;
  p_msg += sprintf( p_msg, "{ \"msg_id\":%li,",        msg_id );
  p_msg += sprintf( p_msg, "\"uptime\":%lu,",          (uint32_t)system_uptime_s() ); 
  p_msg += sprintf( p_msg, "\"system_time\":\"%s\",", get_system_time_str() );
  p_msg += sprintf( p_msg, "\"tank_temp\":\"%0.1f\"", hardware_get_latest_temp_reading_f() );  
  p_msg += sprintf( p_msg, "}" );
  
  return status_msg;
}

//-----------------------------------------------------------------------------
char const * application_get_mqtt_ato_msg(void)
{
#define BOOL_TO_STRING( val )             ( val ? "True" : "False" )
#define LEVEL_SENSOR_TO_STRING( val )     ( val ? "Wet" : "Dry" )
#define SOLENOID_STATE_TO_STRING( val )   ( val ? "Open" : "Closed" )
  
  static char status_msg[256];
  static uint32_t msg_id = 0;
  
  char *p_msg = status_msg;
  msg_id++;
  p_msg += sprintf( p_msg, "{ \"msg_id\":%li,",   msg_id );
  
  p_msg += sprintf( p_msg, "\"manual_control\":\"%s\",",    BOOL_TO_STRING( s_manual_control_enabled ) );
  p_msg += sprintf( p_msg, "\"dt_level_sensor\":\"%s\",",   LEVEL_SENSOR_TO_STRING(s_p_dt->level_sensor_wet) );
  p_msg += sprintf( p_msg, "\"qt_level_sensor\":\"%s\",",   LEVEL_SENSOR_TO_STRING(s_p_qt->level_sensor_wet) );
  p_msg += sprintf( p_msg, "\"dt_water_level\":\"%0.1f\",", s_p_dt->water_level_cm );
  p_msg += sprintf( p_msg, "\"qt_water_level\":\"%0.1f\",", s_p_qt->water_level_cm );
  p_msg += sprintf( p_msg, "\"dt_solenoid\":\"%s\",",       SOLENOID_STATE_TO_STRING( s_p_dt->fill_solenoid.is_open ) );
  p_msg += sprintf( p_msg, "\"qt_solenoid\":\"%s\"",        SOLENOID_STATE_TO_STRING( s_p_qt->fill_solenoid.is_open ) );
  
  p_msg += sprintf( p_msg, "}" );
  
  return status_msg;
}

//-----------------------------------------------------------------------------
char const * application_get_mqtt_wc_msg(void)
{
#define BOOL_TO_STRING( val )             ( val ? "True" : "False" )
#define LEVEL_SENSOR_TO_STRING( val )     ( val ? "Wet" : "Dry" )
#define VALVE_STATE_TO_STRING( val )      ( val ? "Open" : "Closed" )
#define PUMP_STATE_TO_STRING( val )       ( val ? "Running" : "Idle" )
  
  static char status_msg[256];
  static uint32_t msg_id = 0;
  
  float now = system_uptime_s();
  
  char *p_msg = status_msg;
  msg_id++;
  p_msg += sprintf( p_msg, "{ \"msg_id\":%li,",   msg_id );
  
  p_msg += sprintf( p_msg, "\"dt_wc_in_progress\":\"%s\",",   BOOL_TO_STRING( s_p_dt->water_change_end_time_s > 0 ) );
  p_msg += sprintf( p_msg, "\"qt_wc_in_progress\":\"%s\",",   BOOL_TO_STRING( s_p_qt->water_change_end_time_s > 0 ) );
  p_msg += sprintf( p_msg, "\"dt_wc_time_left\":\"%0.1f\",",  fmax( 0, s_p_dt->water_change_end_time_s - now ) );
  p_msg += sprintf( p_msg, "\"qt_wc_time_left\":\"%0.1f\",",  fmax( 0, s_p_qt->water_change_end_time_s - now ) );
  p_msg += sprintf( p_msg, "\"dt_water_level\":\"%0.1f\",",   s_p_dt->water_level_cm );
  p_msg += sprintf( p_msg, "\"qt_water_level\":\"%0.1f\",",   s_p_qt->water_level_cm );
  p_msg += sprintf( p_msg, "\"dt_dump_valve\":\"%s\",",       VALVE_STATE_TO_STRING( s_p_dt->dump_valve_is_open ) );
  p_msg += sprintf( p_msg, "\"qt_dump_pump\":\"%s\"",         PUMP_STATE_TO_STRING( s_p_qt->dump_pump_running ) );
  
  p_msg += sprintf( p_msg, "}" );
  
  return status_msg;
}

//-----------------------------------------------------------------------------
void application_handle_mqtt_request_msg( char *p_msg )
{
  print("MQTT request message data: %s\n", p_msg );

// Message:
//  {
//    "manual_control": "off",
//    "wc_pump_on": "off",
//    "open_dt_solenoid": "off",
//    "open_qt_solenoid": "off"
//  }
  
  JSON_Value  * root_value  = NULL;
  JSON_Object * root_object = NULL;

  root_value =  json_parse_string( p_msg );
  root_object = json_value_get_object( root_value );
  
  // Manual Control requests
  if ( json_object_has_value_of_type( root_object, "manual_control", JSONString ) )
  {
    // Manual control - validate other parameters
    if ( !json_object_has_value_of_type( root_object, "open_dt_solenoid", JSONString ) ||
         !json_object_has_value_of_type( root_object, "open_qt_solenoid", JSONString ) ||
         !json_object_has_value_of_type( root_object, "open_dt_dump_valve", JSONString ) ||
         !json_object_has_value_of_type( root_object, "run_qt_dump_pump", JSONString ) )
    {
      goto func_exit;
    }

    // Anything but an "on" value will be treated as an "off" value
    s_manual_control_enabled = !strcmp( json_object_get_string( root_object, "manual_control" ), "on" );
    s_p_dt->manual_control_open_solenoid_request     = !strcmp( json_object_get_string( root_object, "open_dt_solenoid" ), "on" );
    s_p_qt->manual_control_open_solenoid_request  = !strcmp( json_object_get_string( root_object, "open_qt_solenoid" ), "on" );
    s_p_dt->manual_control_dump_valve_open_request   = !strcmp( json_object_get_string( root_object, "open_dt_dump_valve" ), "on" );
    s_p_dt->manual_control_dump_pump_on_request   = !strcmp( json_object_get_string( root_object, "run_dt_dump_pump" ), "on" );
    s_p_qt->manual_control_dump_pump_on_request   = !strcmp( json_object_get_string( root_object, "run_qt_dump_pump" ), "on" );

    print( "Manual control message: enabled: %i, pumps: dt-%i, qt-%i, solenoids: dt-%i, qt-%i, dump valves:  dt-%i, qt-%i\n",
            s_manual_control_enabled,
            s_p_dt->manual_control_dump_pump_on_request,
            s_p_qt->manual_control_dump_pump_on_request,
            s_p_dt->manual_control_open_solenoid_request,
            s_p_qt->manual_control_open_solenoid_request,
            s_p_dt->manual_control_dump_valve_open_request,
            s_p_qt->manual_control_dump_valve_open_request );
  }

  // Water Change requests
  if ( json_object_has_value_of_type( root_object, "water_change", JSONString ) )
  {
    // Anything but an "start" value will be treated as an "off" value
    if ( !strcmp( json_object_get_string( root_object, "water_change" ), "start" ) )
    {
      // Validate other parameters
      if ( !json_object_has_value_of_type( root_object, "tank", JSONString ) || 
           !json_object_has_value_of_type( root_object, "duration_s", JSONNumber ) )
      {
        goto func_exit;
      }
      
      uint16_t duration_s = json_object_get_number( root_object, "duration_s" );

      bool wc_dt = !strcmp( json_object_get_string( root_object, "tank" ), "DT" );
      bool wc_qt = !strcmp( json_object_get_string( root_object, "tank" ), "QT" );
      bool both_tanks = !strcmp( json_object_get_string( root_object, "tank" ), "Both" );
      
      if ( wc_dt || both_tanks )
      {
        s_p_dt->water_change_request_start      = true;
        s_p_dt->water_change_request_duration_s = duration_s;
      }
      
      if ( wc_qt || both_tanks )
      {
        s_p_qt->water_change_request_start      = true;
        s_p_qt->water_change_request_duration_s = duration_s;
      }
    }
    else
    {
      s_p_dt->water_change_request_stop    = true;
      s_p_qt->water_change_request_stop = true;
    }

    print( "Water change DT: Start requested: %i, Stop requested: %i, duration: %i\n",
            s_p_dt->water_change_request_start,
            s_p_dt->water_change_request_stop,
            s_p_dt->water_change_request_duration_s );
            
    print( "Water change QT: Start requested: %i, Stop requested: %i, duration: %i\n",
            s_p_qt->water_change_request_start,
            s_p_qt->water_change_request_stop,
            s_p_qt->water_change_request_duration_s );
  }


func_exit:
  if ( root_value )
  {
    json_value_free(root_value);
  }
}

//-----------------------------------------------------------------------------
void application_handle_user_button_press(void)
{
}

//-----------------------------------------------------------------------------
static void _turn_on_dump_pump( uint8_t tank_id )
{
  if ( s_tank[tank_id].dump_pump_relay_ctl != INVALID_HARDWARE_HANDLE )
  {
    print( "Turning on Dump Pump\n" );
    hardware_relay_close( s_tank[tank_id].dump_pump_relay_ctl );
    s_tank[tank_id].dump_pump_running = true;
  }
}

//-----------------------------------------------------------------------------
static void _turn_off_dump_pump( uint8_t tank_id )
{
  if ( s_tank[tank_id].dump_pump_relay_ctl != INVALID_HARDWARE_HANDLE )
  {
    print( "Turning off Dump Pump\n" );
    hardware_relay_open( s_tank[tank_id].dump_pump_relay_ctl );
    s_tank[tank_id].dump_pump_running = false;
  }
}

//-----------------------------------------------------------------------------
static void _open_dump_valve( tank_id_t tank_id )
{
  if ( s_tank[tank_id].dump_valve_relay_ctl != INVALID_HARDWARE_HANDLE )
  {  
    print( "Opening %s dump valve\n", TANK_ID_TO_STR( tank_id ) );
    hardware_relay_close( s_tank[tank_id].dump_valve_relay_ctl );
    s_tank[tank_id].dump_valve_is_open = true;
  }
}

//-----------------------------------------------------------------------------
static void _close_dump_valve( tank_id_t tank_id )
{
  if ( s_tank[tank_id].dump_valve_relay_ctl != INVALID_HARDWARE_HANDLE )
  {  
    print( "Closing %s dump valve\n", TANK_ID_TO_STR( tank_id ) );
    hardware_relay_open( s_tank[tank_id].dump_valve_relay_ctl );
    s_tank[tank_id].dump_valve_is_open = false;
  }
}

//-----------------------------------------------------------------------------
static void _open_fill_solenoid( tank_id_t tank_id )
{
  print( "Opening %s fill solenoid\n", TANK_ID_TO_STR( tank_id ) );
  hardware_relay_close( s_tank[tank_id].fill_solenoid.relay_ctl );
  s_tank[tank_id].fill_solenoid.is_open          = true;
  s_tank[tank_id].fill_solenoid.last_open_time_s = system_uptime_s();
}

//-----------------------------------------------------------------------------
static void _close_fill_solenoid( tank_id_t tank_id )
{
  print( "Closing %s fill solenoid\n", TANK_ID_TO_STR( tank_id ) );
  hardware_relay_open( s_tank[tank_id].fill_solenoid.relay_ctl );
  s_tank[tank_id].fill_solenoid.is_open           = false;
  s_tank[tank_id].fill_solenoid.last_close_time_s = system_uptime_s();
}
