#include <driver/adc.h>
#include <driver/dac.h>
#include <driver/ledc.h>
#include <driver/gpio.h>

#include "utils.h"
#include "hardware.h"

#include "ds18b20.h"
#include "vl53l0x.h"

#define PIN_BUTTON                (  0 )
#define PIN_LED                   ( 14 )

typedef struct
{
  uint8_t     enable_gpio;
  uint8_t     i2c_address;
  bool        initialized;
  double      last_measurement_mm; 
  double      tube_length_mm;
  VL53L0X_Dev_t vl53l0x;
} laser_level_ctx_t;

static laser_level_ctx_t s_laser_levels[] = 
{
  // All laser levels come out of reset with i2c address 0x29.  Gets reassigned to below values during init
  [LASER_LEVEL_0] = { .enable_gpio = GPIO_NUM_1, .i2c_address = 0x30, .initialized = false, .tube_length_mm = 444.5 },
  [LASER_LEVEL_1] = { .enable_gpio = GPIO_NUM_2, .i2c_address = 0x31, .initialized = false, .tube_length_mm = 444.5 },
  [LASER_LEVEL_2] = { .enable_gpio = GPIO_NUM_3, .i2c_address = 0x32, .initialized = false, .tube_length_mm = 444.5 },
};

static uint8_t s_relay_ctl_gpio[] = 
{
  [RELAY_CTL_0] = GPIO_NUM_4,
  [RELAY_CTL_1] = GPIO_NUM_5,
  [RELAY_CTL_2] = GPIO_NUM_6,
  [RELAY_CTL_3] = GPIO_NUM_7,
  [RELAY_CTL_4] = GPIO_NUM_8,
  [RELAY_CTL_5] = GPIO_NUM_9,
};

static uint8_t s_fet_ctl_gpio[] = 
{
  [FET_CTL_0] = GPIO_NUM_10,
  [FET_CTL_1] = GPIO_NUM_11,
  [FET_CTL_2] = GPIO_NUM_12,
  [FET_CTL_3] = GPIO_NUM_13,
};

static uint8_t s_level_sensor_gpio[] = 
{
  [LEVEL_SENSOR_0] = GPIO_NUM_38,
  [LEVEL_SENSOR_1] = GPIO_NUM_37,
  [LEVEL_SENSOR_2] = GPIO_NUM_36,
  [LEVEL_SENSOR_3] = GPIO_NUM_35,
  [LEVEL_SENSOR_4] = GPIO_NUM_34,
  [LEVEL_SENSOR_5] = GPIO_NUM_33,
};

static uint8_t s_optical_sensor_gpio[] = 
{
  [OPTICAL_SENSOR_0] = GPIO_NUM_26,
  [OPTICAL_SENSOR_1] = GPIO_NUM_21,
};

static uint8_t s_aux_io_gpio[] = 
{
  [AUX_IO_0] = GPIO_NUM_15,
  [AUX_IO_1] = GPIO_NUM_16,
  [AUX_IO_2] = GPIO_NUM_20,
  [AUX_IO_3] = GPIO_NUM_19,
};

static uint8_t s_dac_out_gpio[] = 
{
  // WARNING: Kicad symbol for ESP32-S2-WROOM in KiCad is wrong, has DAC 1 & 2 backwards
  [DAC_OUT_0] = DAC_CHANNEL_2,
  [DAC_OUT_1] = DAC_CHANNEL_1,
};

static bool   s_led_on = false;
static double s_dac_lsb_v;

static double s_latest_temp_reading_f;

static void _sensors_task(void *Param);

//-----------------------------------------------------------------------------
static void _get_tank_temperatures(void)
{
  static bool do_init           = true;
  static bool temp_sensor_found = false;

  static DS18B20_Info device;
  static owb_rmt_driver_info rmt_driver_info;
  static OneWireBus *owb;
  
  if ( do_init )
  {
    // Create a 1-Wire bus, using the RMT timeslot driver
    owb = owb_rmt_initialize(&rmt_driver_info, s_aux_io_gpio[PIN_TEMP_SENSOR], RMT_CHANNEL_1, RMT_CHANNEL_0);
    owb_use_crc(owb, true);  // enable CRC check for ROM code
    do_init = false;
  }

  if ( !temp_sensor_found )
  {
    OneWireBus_SearchState  search_state = {0};
    owb_search_first(owb, &search_state, &temp_sensor_found);
    if ( temp_sensor_found )
    {
      print("Temp sensor found on OWB\n");
      ds18b20_init_solo(&device, owb);          // only one device on bus
      ds18b20_use_crc(&device, true);           // enable CRC check on all reads
      ds18b20_set_resolution(&device, DS18B20_RESOLUTION_12_BIT);
    }
  }
  
  if ( temp_sensor_found )
  {
    ds18b20_convert_all(owb);

    // In this application all devices use the same resolution,
    // so use the first device to determine the delay
    ds18b20_wait_for_conversion(&device);

    // Read the results immediately after conversion otherwise it may fail
    // (using printf before reading may take too long)
    float reading_c = 0;
    ds18b20_read_temp(&device, &reading_c);

    s_latest_temp_reading_f = ( reading_c * 9 / 5 ) + 32;
  }
}

//-----------------------------------------------------------------------------
static void _get_tank_water_levels(void)
{
  static bool do_init = true;
  
  if ( do_init )
  {
    // Initialize each laser level
    for ( uint8_t idx = 0; idx < ARRAY_SIZE(s_laser_levels); idx++ )
    {
      laser_level_ctx_t *p_level = &s_laser_levels[idx];
      p_level->initialized = ( VL53L0X_Device_init(&p_level->vl53l0x, p_level->enable_gpio, p_level->i2c_address) == VL53L0X_DEVICEERROR_NONE );
      //print( "Laser Level %i init result: %i\n", idx, p_level->initialized );
    }
    
    do_init = false;
  }
  
  uint8_t laser_cnt = ARRAY_SIZE(s_laser_levels);
  double result_mm_sum[laser_cnt];
  uint8_t sample_cnt = 0;
  
  for ( sample_cnt = 0; sample_cnt < 3; sample_cnt++ )
  {
    for ( uint8_t idx = 0; idx < laser_cnt; idx++ )
    {
      laser_level_ctx_t *p_level = &s_laser_levels[idx];
      if ( !p_level->initialized )
      {
        continue;
      }

      uint16_t distance_mm = 0;
      VL53L0X_Device_getMeasurement(&p_level->vl53l0x, &distance_mm);// == VL53L0X_ERROR_NONE
      
      result_mm_sum[idx] = ( sample_cnt > 0 ? result_mm_sum[idx] : 0 ) + distance_mm;
    }
  }

  for ( uint8_t idx = 0; idx < laser_cnt; idx++ )
  {
    laser_level_ctx_t *p_level = &s_laser_levels[idx];
    if ( !p_level->initialized )
    {
      continue;
    }
    
    p_level->last_measurement_mm = fmax( 0, p_level->tube_length_mm - ( result_mm_sum[idx] / sample_cnt ) );
//    print("Laser %i range: %.1f mm\n", idx, p_level->last_measurement_mm );
  }
}

//-----------------------------------------------------------------------------
static void _sensors_task(void *Param)
{
  TickType_t last_wake_time = xTaskGetTickCount();

  while (1)
  {   
    _get_tank_temperatures();
    _get_tank_water_levels();
    
    #define SAMPLE_PERIOD        (100)   // milliseconds
    vTaskDelayUntil(&last_wake_time, SAMPLE_PERIOD / portTICK_PERIOD_MS);
  }
}

//-----------------------------------------------------------------------------
double hardware_get_latest_temp_reading_f()
{
  return s_latest_temp_reading_f;
}


//-----------------------------------------------------------------------------
void hardware_init()
{
  gpio_config_t io_conf;
  
  //----------------------------------------------------
  // Inputs with pullup:
  io_conf.mode          = GPIO_MODE_INPUT;
  io_conf.intr_type     = GPIO_INTR_DISABLE;
  io_conf.pull_up_en    = 1;
  io_conf.pull_down_en  = 0;
  io_conf.pin_bit_mask  = 0;

  // Button Pin
  io_conf.pin_bit_mask  |= ( 1ULL << PIN_BUTTON );

  gpio_config(&io_conf);
  
  //----------------------------------------------------
  // Inputs without pullup:
  io_conf.mode          = GPIO_MODE_INPUT;
  io_conf.intr_type     = GPIO_INTR_DISABLE;
  io_conf.pull_up_en    = 0;
  io_conf.pull_down_en  = 0;
  io_conf.pin_bit_mask  = 0;
  
  // Set Level Sense Input pins
  for ( uint8_t idx = 0; idx < ARRAY_SIZE(s_level_sensor_gpio); idx++ )
  {
    io_conf.pin_bit_mask |= ( 1ULL << s_level_sensor_gpio[idx] );
  }
  
  // Set Optical Sense Input pins
  for ( uint8_t idx = 0; idx < ARRAY_SIZE(s_optical_sensor_gpio); idx++ )
  {
    io_conf.pin_bit_mask |= ( 1ULL << s_optical_sensor_gpio[idx] );
  }
  
  gpio_config(&io_conf);
  
  //----------------------------------------------------
  // Outputs:
  io_conf.mode          = GPIO_MODE_OUTPUT;
  io_conf.intr_type     = GPIO_INTR_DISABLE;
  io_conf.pull_up_en    = 0;
  io_conf.pull_down_en  = 0;
  io_conf.pin_bit_mask  = 0;

  // Set the LED pin to output
  io_conf.pin_bit_mask = ( 1ULL << PIN_LED );

  // Set Laser Level reset pins to output
  for ( uint8_t idx = 0; idx < ARRAY_SIZE(s_laser_levels); idx++ )
  {
    io_conf.pin_bit_mask |= ( 1ULL << s_laser_levels[idx].enable_gpio );
  }
  
  // Set Relay Controls to outputs
  for ( uint8_t idx = 0; idx < ARRAY_SIZE(s_relay_ctl_gpio); idx++ )
  {
    io_conf.pin_bit_mask |= ( 1ULL << s_relay_ctl_gpio[idx] );
  }
  
  // Set FET Controls to outputs
  for ( uint8_t idx = 0; idx < ARRAY_SIZE(s_fet_ctl_gpio); idx++ )
  {
    io_conf.pin_bit_mask |= ( 1ULL << s_fet_ctl_gpio[idx] );
  }
  
  gpio_config(&io_conf);  
   
  //----------------------------------------------------
  // DAC Outputs:
  for ( uint8_t idx = 0; idx < ARRAY_SIZE(s_dac_out_gpio); idx++ )
  {
    dac_output_enable(s_dac_out_gpio[idx]);
    dac_output_voltage(s_dac_out_gpio[idx], 0);
  }
  s_dac_lsb_v = 3.3 / 255;
  
  xTaskCreate( _sensors_task, "sensors_task", 1024 * 3, NULL, 5, NULL);
}

//-----------------------------------------------------------------------------
double hardware_get_dac_max_voltage(void)
{
  return 3.3;
}

//-----------------------------------------------------------------------------
void hardware_set_dac_voltage(dac_out_t dac_out, double voltage)
{
  uint8_t dac_raw_val = (uint8_t)(voltage / s_dac_lsb_v);
  print("Setting DAC %i voltage: %f (%i)\n", dac_out, voltage, dac_raw_val);
  dac_output_voltage(s_dac_out_gpio[dac_out], dac_raw_val);
}

//-----------------------------------------------------------------------------
bool hardware_user_button_pressed(void)
{
  return !gpio_get_level( PIN_BUTTON );                             // Active low
}

//-----------------------------------------------------------------------------
bool hardware_optical_sensor_is_wet(optical_sensor_t optical_sensor)
{
  return !gpio_get_level(s_optical_sensor_gpio[optical_sensor]);    // Active low
}

//-----------------------------------------------------------------------------
bool hardware_level_sensor_is_wet(level_sensor_t level_sensor)
{
  return gpio_get_level( s_level_sensor_gpio[level_sensor]);      // Active high
}

//-----------------------------------------------------------------------------
void hardware_turn_on_led(void)
{
  gpio_set_level( PIN_LED, 1 );
  s_led_on = true;
}

//-----------------------------------------------------------------------------
void hardware_turn_off_led(void)
{
  gpio_set_level( PIN_LED, 0 );
  s_led_on = false;
}

//-----------------------------------------------------------------------------
void hardware_toggle_led(void)
{
  s_led_on = !s_led_on;
  gpio_set_level( PIN_LED, s_led_on );
}

//-----------------------------------------------------------------------------
void hardware_relay_open(relay_ctl_t relay_ctl)
{
  if ( relay_ctl == INVALID_HARDWARE_HANDLE )
  {
    return;
  }
  gpio_set_level( s_relay_ctl_gpio[relay_ctl], 0 );
}

//-----------------------------------------------------------------------------
void hardware_relay_close(relay_ctl_t relay_ctl)
{
  if ( relay_ctl == INVALID_HARDWARE_HANDLE )
  {
    return;
  }
  gpio_set_level( s_relay_ctl_gpio[relay_ctl], 1 );
}

//-----------------------------------------------------------------------------
void hardware_fet_energize(fet_ctl_t fet_ctl)
{
  if ( fet_ctl == INVALID_HARDWARE_HANDLE )
  {
    return;
  }
  gpio_set_level( s_fet_ctl_gpio[fet_ctl], 1 );
}

//-----------------------------------------------------------------------------
void hardware_fet_open(fet_ctl_t fet_ctl)
{
  if ( fet_ctl == INVALID_HARDWARE_HANDLE )
  {
    return;
  }
  gpio_set_level( s_fet_ctl_gpio[fet_ctl], 0 );
}

//-----------------------------------------------------------------------------
double hardware_get_laser_level_mm( laser_level_t idx )
{
  if ( ( idx == INVALID_HARDWARE_HANDLE ) || ( !s_laser_levels[idx].initialized ) )
  {
    return 0;
  }
  return s_laser_levels[idx].last_measurement_mm;
}
