To add VL5310x stuff, from this directory:

  git clone git@github.com:KWolfe81/vl53l0x_esp32.git
  
  cd ..
  idf.py menuconfig
  --> Component config
      --> VL5310x API
          --> SDA Pin
          --> SCL Pin
          
          
VL5310x Wiring: 
Yellow: Ground
Red:    Vcc
White:  SDA   (GPIO3)
Black:  SCL   (GPIO2)
Green:  ENA   (GPIO1)

